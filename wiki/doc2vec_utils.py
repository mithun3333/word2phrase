from gensim.models.doc2vec import Doc2Vec
from numpy import dot
from gensim import matutils

class Doc2VecUtils():
    def __init__(self, path_to_model):
        self.model = Doc2Vec.load(path_to_model)
    
    def capitalize_first(self, word):
        capitalized_term = word.lower()
        capitalized_term = capitalized_term[0].upper() + capitalized_term[1:]
        return capitalized_term
    
    def get_most_similar(self, word):        
        capitalized_term = self.capitalize_first(word)
        try:
            return self.model.docvecs.most_similar(positive=[capitalized_term], topn=10)
        except:
            return "word not in the vocab" 
    
    def get_score(self, doc_word1, doc_word2, alpha=0.1, min_alpha=0.0001, steps=5):
        """@ doc_word1, doc_word2: words
        """
        doc_words1 = doc_word1.lower().split()
        doc_words2 = doc_word2.lower().split()
        try:
            return self.model.n_similarity(doc_words1, doc_words2)
        except:
            return "word not in the vocab"

dv = Doc2VecUtils("wiki.model")
dv.get_most_similar('Machine Learning')
dv.get_score("Machine learning", "Artificial Intelligence")