Download vectors from [here](https://fasttext.cc/docs/en/english-vectors.html)

```commandline
unzip model.zip
```

Add the model in the code directory then run the below command
to get similarity scores of two phrases

```commandline
$ python3
>>> from similarity_utils import SimilarityUtils
>>> s_utils = SimilarityUtils()
>>> s_utils.get_similarity(phrase1, phrase2)
```
