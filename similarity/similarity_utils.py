import numpy as np
import gensim
import random

from nltk.corpus.reader import wordnet
from nltk.stem.snowball import SnowballStemmer

from gensim import utils, matutils
from gensim.models import Word2Vec

from word_utils import get_metaphor_map, get_phrase_map
from wordnet_phrase_generator import Synsets
from conceptnet_utils import ConceptNetUtils

prefix_list = ['a', 'an', 'the']


class SimilarityUtils:
    def __init__(self):
        #self.model = Word2Vec.load_word2vec_format('wiki-news-300d-1M.vec') 
        self.model = Word2Vec.load_word2vec_format('crawl-300d-2M.vec')
        self.metaphor_dict = get_metaphor_map()
        self.phrase_dict = get_phrase_map(self.metaphor_dict)
        self.synset = Synsets()
        self.stemmer = SnowballStemmer("english")
        self.concept_utils = ConceptNetUtils()

    def _sent_vectorizer(self, sent):
        sent_vec = []
        numw = 0
        for w in sent:
            try:
                if numw == 0:
                    sent_vec = self.model[w]
                else:
                    sent_vec = np.add(sent_vec, self.model[w])
                numw += 1
            except:
                pass

        if len(sent_vec) == 0:
            return None
        else:
            return np.asarray(sent_vec) / numw

    def get_similarity_of_vector(self, vector1, vector2):
        return np.dot(matutils.unitvec(vector1), matutils.unitvec(vector2))

    def get_similarity_of_two_phrase(self, phrase1, phrase2, is_in_same_bucket):
        word_list_for_phrase1 = phrase1.split()
        word_list_for_phrase2 = phrase2.split()

        vector1 = self._sent_vectorizer(word_list_for_phrase1)
        vector2 = self._sent_vectorizer(word_list_for_phrase2)

        if vector1 is not None and vector2 is not None:
            if is_in_same_bucket:
                vector2 = (vector1 + vector2)/2

            return self.get_similarity_of_vector(vector1, vector2)
        else:
            return None


    def _nudge_vector(self, vector1, vector2):
        vector2 = (vector1 + vector2)/2
        return vector2

    def similarity_nudged_vector(self, vector1, vector2):
        vector2 = self._nudge_vector(vector1, vector2)
        return self.get_similarity_of_vector(vector1, vector2)


    def synset_similarity(self, phrase1, phrase2):
        phrase1_list = []
        phrase2_list = []
        is_same_bucket = False

        if len(phrase1.split()) == 1:
            phrase1_list = self.synset.get_all_synonyms_phrases(phrase1)
            if len(phrase2.split()) >= 2:
                stemmed_phrase2 = self.stem_phrase(phrase2)
                for phrase in phrase1_list:
                    if len(phrase.split()) >= 2 and len(phrase2.split()) >= 2 and self.stem_phrase(phrase) in stemmed_phrase2:
                        phrase2 = phrase
                        is_same_bucket = True
                        break
                    elif len(phrase.split()) >= 2 and len(phrase2.split()) == 2 and self.stem_phrase(phrase) == stemmed_phrase2:
                        phrase2 = phrase
                        is_same_bucket = True
                        break
                                

        if len(phrase2.split()) == 1:
            phrase2_list = self.synset.get_all_synonyms_phrases(phrase2)
            if len(phrase1.split()) >= 2:
                stemmed_phrase1 = self.stem_phrase(phrase1)
                for phrase in phrase2_list:
                    if len(phrase.split()) >= 2 and len(phrase1.split()) >= 2 and self.stem_phrase(phrase) in stemmed_phrase1:
                        phrase1 = phrase
                        is_same_bucket = True
                        break
                    elif len(phrase.split()) >= 2 and len(phrase1.split()) == 2 and self.stem_phrase(phrase) == stemmed_phrase1:
                        phrase2 = phrase
                        is_same_bucket = True
                        break

        if len(phrase1_list) == 0:
            phrase1_list = [phrase1]

        if len(phrase2_list) == 0:
            phrase2_list = [phrase2]


        if is_same_bucket:
            return 1.0
        else:
            max_score = 0
            if len(phrase1.split()) == 1 and len(phrase2.split()) == 1:
                try:
                    score = self.synset.get_wup_similarity_nouns(phrase1, phrase2)
                except wordnet.WordNetError:
                    score = 0

                if score > 0.5:
                    max_score = score
            return max_score

    def get_similarity(self, phrase1, phrase2):

        phrase1 = phrase1.lower()
        phrase2 = phrase2.lower()

        actual_phrase1 = phrase1
        actual_phrase2 = phrase2

        phrase1_list = []
        phrase2_list = []

        if phrase1 in self.metaphor_dict.keys():
            phrase1_list = self.metaphor_dict[phrase1]
        elif len(phrase1.split()) > 2:
            for key in self.metaphor_dict.keys():
                if key in phrase1:
                    phrase1 = key
                    phrase1_list = self.metaphor_dict[key]
                    break

        elif len(phrase1.split()) == 2:
            words = phrase1.split()
            if words[0] in prefix_list:
                phrase1 = words[1]
                phrase1_list = [phrase1]

        if len(phrase1_list) == 0:
            phrase1_list = [phrase1]

        if phrase2 in self.metaphor_dict.keys():
            phrase2_list = self.metaphor_dict[phrase2]
        elif len(phrase2.split()) > 2:
            for key in self.metaphor_dict.keys():
                if key in phrase2:
                    phrase2 = key
                    phrase2_list = self.metaphor_dict[key]
                    break

        elif len(phrase2.split()) == 2:
            words = phrase2.split()
            if words[0] in prefix_list:
                phrase2 = words[1]
                phrase2_list = [phrase2]

        if len(phrase2_list) == 0:
            phrase2_list = [phrase2]

        is_in_same_bucket = False

        keys1 = []
        keys2 = []

        if phrase1 != phrase2:
            if len(phrase1_list) == 1 and phrase1_list[0] == actual_phrase1:
                if phrase1 in self.phrase_dict.keys():
                    keys1 = self.phrase_dict[phrase1]

                elif len(phrase1.split()) > 2:
                    for key in self.phrase_dict.keys():
                        if len(key.split()) > 1 and key in phrase1:
                            keys1 = self.phrase_dict[key]
                            phrase1 = key
                            break

            if len(phrase2_list) == 1 and phrase2_list[0] == actual_phrase2:
                if phrase2 in self.phrase_dict.keys():
                    keys2 = self.phrase_dict[phrase2]
                elif len(phrase2.split()) > 2:
                    for key in self.phrase_dict.keys():
                        if len(key.split()) > 1 and key in phrase2:
                            keys2 = self.phrase_dict[key]
                            phrase2 = key
                            break

        intersect = list(set(keys1) & set(keys2))
        if len(intersect) > 0:
            is_in_same_bucket = True

        if is_in_same_bucket:
            max_score = 1.0 #self.get_similarity_of_two_phrase(phrase1, phrase2, is_in_same_bucket)
        else:
            max_score = 0
            for phrase1 in phrase1_list:
                for phrase2 in phrase2_list:
                    score = self.get_similarity_of_two_phrase(phrase1, phrase2, False)
                    if score is not None and score > max_score:
                        max_score = score


            score1 = self.synset_similarity(phrase1, phrase2)
            score2 = self.concept_utils.similarity_score(phrase1, phrase2)
            max_score = max(max_score, score1, score2)           
          
        if int(max_score) >= 1:
            max_score = random.randint(9400,9900)*0.0001

        return max_score


    def stem_phrase(self, phrase):
        stemmed_words = []
        words = phrase.split()
        for w in words:
            stemmed_words.append(self.stemmer.stem(w))
            
        return " ".join(stemmed_words)
