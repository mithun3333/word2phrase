def get_metaphor_map(fname='cleanPhrases.txt'):
    metaphor_dict = dict()

    with open(fname, 'r') as file:
        for line in file:
            tokens = line.strip().split('|')
            tokens_len = len(tokens)
            if tokens_len > 2:
                key = tokens[0].strip().lower()
                value = [w.strip().lower() for w in tokens[2:]]
                metaphor_dict[key] = value
    return metaphor_dict

def get_phrase_map(metaphor_dict):
    phrases_dict = {}
    for key in metaphor_dict.keys():
        for phrase in metaphor_dict[key]:
            if phrases_dict.get(phrase) is None:
                phrases_dict[phrase] = [key]
            else:
                phrases_dict[phrase].append(key)

    return phrases_dict
