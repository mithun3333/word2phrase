import requests

from nltk.stem.snowball import SnowballStemmer

class ConceptNetUtils:

    def __init__(self):
        self.url = "http://api.conceptnet.io/c/en/"
        self.stemmer = SnowballStemmer("english")

    def extract(self, edge_dict, word):
        start = edge_dict['start']['label'].lower()
        end = edge_dict['end']['label'].lower()
        if word not in list(start.split()):
            return start
        else:
            return end

    def get_phrases(self, word):
        obj = requests.get(self.url + word).json()
        result_dict = {}
        phrase_list = []

        for edge in obj['edges']:
            phrase_tuple = (self.extract(edge, word), edge['weight'])
            phrase_list.append(phrase_tuple)

        result_dict["word"] = word
        result_dict["phrases"] = phrase_list

        return phrase_list

    def stem_phrase(self, phrase):
        stemmed_words = []
        words = phrase.split()
        for w in words:
            stemmed_words.append(self.stemmer.stem(w))
        return " ".join(stemmed_words)

    def similarity_score(self, phrase1, phrase2):
        words_in_phrase1 = phrase1.split()
        words_in_phrase2 = phrase2.split()

        if len(words_in_phrase1) > 1 and len(words_in_phrase2) > 1:
            return 0.0
        else:
            final_score = 0.0
            if len(words_in_phrase1) == 1:
                phrase_list = self.get_phrases(phrase1)
                stemmed_phrase2 = self.stem_phrase(phrase2)
                max_score = 0
                for phrase, score in phrase_list:
                    if len(phrase.split()) >= 2 and len(phrase2.split()) >= 2 and self.stem_phrase(
                            phrase) in stemmed_phrase2:
                        if max_score < score:
                            max_score = score

                    elif len(phrase.split()) >= 2 and len(phrase2.split()) == 2 and self.stem_phrase(
                            phrase) == stemmed_phrase2:
                        if max_score < score:
                            max_score = score

                final_score = max_score/10

            elif len(words_in_phrase2) == 1:
                phrase_list = self.get_phrases(phrase2)
                stemmed_phrase1 = self.stem_phrase(phrase1)
                max_score = 0.0
                for phrase, score in phrase_list:
                    if len(phrase.split()) >= 2 and len(phrase2.split()) >= 2 and self.stem_phrase(
                            phrase) in stemmed_phrase1:
                        if max_score < score:
                            max_score = score

                    elif len(phrase.split()) >= 2 and len(phrase2.split()) == 2 and self.stem_phrase(
                            phrase) == stemmed_phrase1:
                        if max_score < score:
                            max_score = score

                final_score = max_score/10

            if final_score > 0.4:
                return final_score
            else:
                return 0.0
