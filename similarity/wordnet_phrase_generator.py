from nltk.corpus import wordnet as wn

class Synsets(object):
    def get_all_synsets(self, word, pos=None):
        synset = []
        for ss in wn.synsets(word):
            for lemma in ss.lemma_names():
                synset.append((lemma, ss.name()))
        return synset
    
    def get_all_hyponyms(self, word, pos=None):
        hypos = []
        for ss in wn.synsets(word, pos=pos):
            for hyp in ss.hyponyms():
                for lemma in hyp.lemma_names():
                    hypos.append((lemma, hyp.name()))
        return hypos
    
    def get_all_similar_tos(self, word, pos=None):
        similars = []
        for ss in wn.synsets(word):
            for sim in ss.similar_tos():
                for lemma in sim.lemma_names():
                    similars.append((lemma, sim.name()))
        return similars
    
    def get_all_also_sees(self, word, pos=None):
        see_alsos = []
        for ss in wn.synsets(word):
            for also in ss.also_sees():
                for lemma in also.lemma_names():
                    see_alsos.append((lemma, also.name()))
        return see_alsos
    
    def get_all_synonyms_phrases(self, word, pos=None):
        agg_syns = []
        syns = self.get_all_synsets(word, pos) + self.get_all_hyponyms(word, pos) + self.get_all_similar_tos(word, pos) + self.get_all_also_sees(word, pos)

        filtered_syns = []
        for syn in syns:
            if '_' in syn[0]:
               filtered_syns.append(syn[0].replace('_', ' '))
        return filtered_syns
    
    def get_all_synonyms(self, word, pos=None):
        agg_syns = []
        syns = self.get_all_synsets(word, pos) + self.get_all_hyponyms(word, pos) + self.get_all_similar_tos(word, pos) + self.get_all_also_sees(word, pos)

        filtered_syns = []
        for syn in syns:
           filtered_syns.append(syn[0].replace('_', ' '))
        return filtered_syns
    
    def get_wup_similarity_nouns(self, word1, word2):
        word1_synset = wn.synset(word1 + '.n.01')
        word2_synset = wn.synset(word2 + '.n.01')
        return word1_synset.wup_similarity(word2_synset)
