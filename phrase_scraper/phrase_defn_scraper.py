import requests
from bs4 import BeautifulSoup
import csv
import re

r = requests.get(
    "https://www.phrases.org.uk/meanings/phrases-and-sayings-list.html")
soup = BeautifulSoup(r.content, "lxml")


p_tags = soup.find_all("p", {"class": "phrase-list"})

with open('phrase_meaning_words.csv', 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter='|')
        for p_tag in p_tags:
                link = p_tag.find('a')['href']
                gotolink = requests.get(
                    "https://www.phrases.org.uk/meanings/"+link)
                soup = BeautifulSoup(gotolink.content, "lxml")
                other_words = soup.find_all("p", {"class": "meanings-body"})
                word = p_tag.text.replace("\n", " ").encode('utf-8')
                word = re.sub("\s+", " ", word)
                if other_words:
                        meaning = other_words[0].text.replace(
                            "\n", " ").encode('utf-8')
                        row = [word.strip()] + [meaning.strip()]
                else:
                        row = [word.strip()]
                writer.writerow(row)
                csvfile.flush()
