import csv
import spacy

nlp=spacy.load('en_core_web_sm')

with open("phrase_NP_words.csv", 'w') as writefile:
        writer = csv.writer(writefile, delimiter='|')
        with open("phrase_meaning_words.csv", 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter='|')
                for row in reader:
                        if len(row) == 2:
                                doc = nlp(row[1].decode("utf-8", "ignore"))
                                writer.writerow([row[0]] + [row[1]] + [np.text.encode("utf-8") for np in doc.noun_chunks])

