import csv
import requests
from bs4 import BeautifulSoup
from time import sleep
import argparse
import urllib
from urllib.error import HTTPError

class PowerThesaurusScrape:
    def __init__(self):
        self.web_address = 'https://www.powerthesaurus.org/'
        self.web_address_additional = '/synonyms/expression'

    def check_expressions_presence(self, word):
        found = self.get_main_title(word)
        session = requests.Session()
        session.max_redirects = 50
        print(self.web_address + word + self.web_address_additional)
        try:
            page_1 = session.get(self.web_address + word + self.web_address_additional)
            sleep(2.5)
            soup_1 = BeautifulSoup(page_1.text, 'html.parser')
            sleep(0.5)
            sidebars_list = []
            sidebar_1 = soup_1.find_all('div', {'class': 'pt-sidebar__wrapper'})
            if found == True:
                for side in sidebar_1:
                    items = [line.strip() for line in side.text.strip().split('\n') if line.strip != '']
                    sidebars_list += items
                if 'expressions' in sidebars_list:
                    found = True
                else:
                    found = False
            return found
        except (HTTPError, requests.exceptions.TooManyRedirects, requests.exceptions.ConnectionError):
            return False


    def get_main_title(self, word):
        # page = requests.get(self.web_address + word + self.web_address_additional)
        try:
            session = requests.Session()
            session.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
            session.max_redirects = 50
            # print(self.web_address + word + self.web_address_additional)
            page = session.get(self.web_address + word + self.web_address_additional)
            sleep(2.5)
            soup = BeautifulSoup(page.text, 'html.parser')
            sleep(0.3)
            notification_bar = soup.find_all('h1', {'class': 'pt-title__main'})
            for notifications in notification_bar:
                if notifications.text.strip() == word:
                        return True
                else:
                        return False
        except (HTTPError, requests.exceptions.TooManyRedirects, requests.exceptions.ConnectionError):
            return False



    def get_phrases_scores(self, word):
        disabled_next_str = '<div class="pt-pagination__button pt-pagination__button--disabled"><span class="pt-pagination__caption">next</span>'
        expressions_list = []
        ratings_list = []
        page_1 = requests.get(self.web_address + word + self.web_address_additional)
        sleep(1.0)
        soup_1 = BeautifulSoup(page_1.text, 'html.parser')
        expressions_1 = soup_1.find_all('div', {'class': 'pt-thesaurus-card__term-title'})
        sleep(0.3)
        ratings_1 = soup_1.find_all('div', {'class': 'pt-thesaurus-card__rating'})
        for exp in expressions_1:
            expressions_list.append(exp.text)
        for rate in ratings_1:
            ratings_list.append(rate.text)

        pt_pagination = soup_1.find_all('div', {'class': 'pt-pagination'})
        if len(pt_pagination) > 0:
            for no_of_pages in range(2,50000):
                page_current = requests.get(self.web_address+word+self.web_address_additional+ '/' + str(no_of_pages))
                soup = BeautifulSoup(page_current.text, 'html.parser')
                pt_pagination = soup.find_all('div', {'class': 'pt-pagination'})
                expressions = soup.find_all('div', {'class': 'pt-thesaurus-card__term-title'})
                ratings = soup.find_all('div', {'class': 'pt-thesaurus-card__rating'})
                for exp in expressions:
                    expressions_list.append(exp.text)
                for rate in ratings:
                    ratings_list.append(rate.text)

                if len(pt_pagination) == 0 or (len(pt_pagination) > 0 and disabled_next_str in str(pt_pagination[0])):
                    break

        return expressions_list, ratings_list

    def write_into_file(self, word, filename):
        print("processing:" + word)
        expressions_list, ratings_list = self.get_phrases_scores(word)
        with open(filename, 'a') as f:
            for i in range(0, len(ratings_list)):
                f.write(expressions_list[i] + '|' + word + '|' + ratings_list[i] + '\n')
        f.close()

    def check_presence_of_number(self, word):
        return any(i.isdigit() for i in word)

    def read_words_from_file(self, file_path, output_file, flag_word=None):
        file = open(file_path, 'r')
        reader = csv.reader(file)
        is_readable = False
        for word in reader:
            if flag_word is None:
                is_readable = True
            elif not is_readable:
                if word[0] == flag_word.strip():
                    is_readable = True

            if is_readable and len(str(word[0].strip()))>1 and word[0].isalpha() and not self.is_repeating(word[0]) and self.is_lower(word[0]):
                if not self.check_presence_of_number(str(word[0].strip())):
                    if self.check_expressions_presence(str(word[0]).strip()):
                        self.write_into_file(str(word[0]).strip(), output_file)

    def is_repeating(self, s):
        return s == len(s) * s[0]

    def is_lower(self, s):
        return s[0].lower() == s[0] and ('-' not in s)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--word_list", default='/Users/mithun/Downloads/words_1.txt')
    parser.add_argument("--phrase_extraction", default='output.txt')
    parser.add_argument("--word")
    args = parser.parse_args()

    PowerThesaurusScrape().read_words_from_file(args.word_list, args.phrase_extraction, args.word)

