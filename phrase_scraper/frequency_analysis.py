#extract word and its count from phrase_NP_words.csv
class Phrase_Frequency_Generator:
    
    _phrase_filename = "phrase_NP_words.csv"
    _words_to_exclude = ['a','an','the','','-','->',';','=']
    
    def word_frequency_count(self):        
        token_list = [] #list of list to store list of words in a list of phrases.
        with open(self._phrase_filename,'r') as reader:
            for line in reader:
                phrase_list = list(line.rstrip('\n').split('|'))[2:]
                for phrase in phrase_list:
                    tokens = phrase.lower().split()
                    token_list.append(tokens)
    
        word_count_dict_ = {} 
    
        for phrase in token_list:
            for token in phrase:
                clean_token = token.rstrip(',').strip('\'').strip('(').strip(')').strip('[').strip(']').strip('\\').strip('/')
                if clean_token not in self._words_to_exclude:
                
                    if clean_token not in word_count_dict_.keys():
                        word_count_dict_[clean_token] = 1
                    else:
                        word_count_dict_[clean_token] += 1
    
        word_count_tuple_list = list(word_count_dict_.items())
    
        word_count_tuple_list.sort(key= lambda x : x[1],reverse = True)
        
        #display the word frequency
        for key, val in word_count_tuple_list :
            string = "{:20s} : {:5d}".format(key,val)
            print(string)
    
        return word_count_tuple_list
    
        


if __name__ == "__main__":
    obj = Phrase_Frequency_Generator()
    word_count_list = obj.word_frequency_count()
    