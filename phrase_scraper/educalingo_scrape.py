import csv
import requests
from bs4 import BeautifulSoup
from time import sleep
import argparse
import random

class EducalingoScrape:
    def __init__(self):
        self.web_address = 'https://educalingo.com/en/dic-en/'
        self.display_text_class = '<h1 class="h1_inicio"><span class="apartado_inicio_titulo_en">The dictionary</span><br/><span class="apartado_inicio_subtitulo_en">for curious people</span></h1>'

    def check_word_exists(self, word):
        page = requests.get(self.web_address + word)
        soup = BeautifulSoup(page.text, 'html.parser')
        sleep(5)
        if self.display_text_class in str(soup):
            return False
        else:
            return True
    def check_presence_of_number(self, word):

        return any(i.isdigit() for i in word)

    def get_phrases(self, word):
        phrases_list = []
        page = requests.get(self.web_address + word)
        soup = BeautifulSoup(page.text, 'html.parser')
        sleep(random.randint(5, 30))
        phrase_def = soup.find_all('div', {'class': 'contenido_sinonimos_antonimos'})
        sleep(5)
        for phrase in phrase_def:
            list_of_phrase = []
            phrase = phrase.text.split(' · ')
            for i in range(len(phrase)):
                list_of_phrase.append(phrase[i])
            for i in range(len(list_of_phrase)):
                if len(list_of_phrase[i].split()) > 1:
                    phrases_list.append(list_of_phrase[i].strip())
        return phrases_list

    def get_figure_of_speech(self, word):
        page = requests.get(self.web_address + word)
        soup = BeautifulSoup(page.text, 'html.parser')
        sleep(random.randint(5, 30))
        speech_def = soup.find_all('div', {'class': 'categoria_gramatical'})
        sleep(5)
        count = 0
        fig_speech = []
        for speech in speech_def:
            count += 1
            if 'div class="circulo_categoria_gramatical background_gris"' not in str(speech):
                if count == 1:
                    fig_speech.append('n')
                if count == 2:
                    fig_speech.append('adj')
                if count == 3:
                    fig_speech.append('v')
                if count == 4:
                    fig_speech.append('adv')
                if count == 5:
                    fig_speech.append('pro')
                if count == 6:
                    fig_speech.append('prep')
                if count == 7:
                    fig_speech.append('conj')
                if count == 8:
                    fig_speech.append('det')
                if count == 9:
                    fig_speech.append('exc')
        return fig_speech

    def write_into_file(self, word, filename):
        phrases_list = self.get_phrases(word)
        fig_speech = self.get_figure_of_speech(word)
        with open(filename, 'a') as f:
            if len(fig_speech) == 1:
                for i in range (0, len(phrases_list)):
                    f.write(phrases_list[i] + '|' + word + '|' + str(fig_speech[0]) + '\n')
            elif len(fig_speech) == 0:
                for i in range(0, len(phrases_list)):
                    f.write(phrases_list[i] + '|' + word + '\n')
            else:
                for i in range (0, len(phrases_list)):
                    expression = phrases_list[i].split(' ')
                    fig_speech = self.get_figure_of_speech('-'.join(expression))
                    if len(fig_speech) == 0:
                        f.write(phrases_list[i] + '|' + word + '\n')
                    else:
                        for j in range (0, len(fig_speech)):
                            f.write(phrases_list[i] + '|' + word + '|' + str(fig_speech[j]) + '\n')
        f.close()

    def read_words_from_file(self, file_path, output_file, flag_word=None):
        file = open(file_path, 'r')
        reader = csv.reader(file)
        is_readable = False
        for word in reader:
            if flag_word is None:
                is_readable = True
            elif not is_readable:
                if word[0] == flag_word.strip():
                    is_readable = True

            if is_readable and  len(str(word[0].strip()))>1:
                if not self.check_presence_of_number(str(word[0].strip())):
                    print('processing: ' + word[0])
                    if self.check_word_exists(str(word[0]).strip()):
                        self.write_into_file(str(word[0]).strip(), output_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--word_list', default = 'words.txt')
    parser.add_argument('--phrase_extraction', default = 'educalingo_output.txt')
    parser.add_argument('--word')
    args = parser.parse_args()

    EducalingoScrape().read_words_from_file(args.word_list, args.phrase_extraction, args.word)